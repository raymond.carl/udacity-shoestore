package com.udacity.shoestore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil


import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels

import com.udacity.shoestore.databinding.FragmentShoeListBinding
import com.udacity.shoestore.databinding.ShoeDetailLayoutBinding

import com.udacity.shoestore.models.ShoeViewModel

import androidx.lifecycle.Observer
import com.udacity.shoestore.models.UserViewModel
import com.udacity.shoestore.shoedetails.ShoeDetailViewModel

import androidx.navigation.fragment.NavHostFragment.findNavController

import timber.log.Timber

class ShoeListFragment : Fragment() {

    /*
    Technique was learned from this stackoverflow thread to access the same instance of the shoe viewmodel from a
    a "child" fragment (or fragment within the same activity) because I don't want to create a new instance
    of the shoe viemodel but we also aren't using repositories yet either. This is not a good pattern, but fits
    ok for this project.
    https://stackoverflow.com/questions/59952673/how-to-get-an-instance-of-viewmodel-in-activity-in-2020-21

    Update: View Model instance is now created in the title fragment so that both the catalogue and the detailed view
    can access the same instance without re-initializing the dummy data or creating a new instance when navigating
    back from the detailed view.

    */

    private val shoeViewModel: ShoeViewModel by viewModels(
        ownerProducer = { requireParentFragment() }
    )

    private lateinit var shoeDetailViewModel: ShoeDetailViewModel

    private val userViewModel : UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentShoeListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shoe_list, container, false)

        binding.lifecycleOwner = this
        binding.shoeViewModel = shoeViewModel


        shoeViewModel.shoes.observe(viewLifecycleOwner, Observer {shoes ->

            for (shoe in shoes) {
                val binding2: ShoeDetailLayoutBinding = DataBindingUtil.inflate(inflater, R.layout.shoe_detail_layout, container, false)

                shoeDetailViewModel = ShoeDetailViewModel(shoe, shoeViewModel)

                binding2.shoe = shoeDetailViewModel
                binding.scrollViewLinearLayout.addView(binding2.root)
                binding2.floatingButton.setOnClickListener{ view : View ->
                    findNavController(this).navigate(ShoeListFragmentDirections.actionShoeListToShoeDetailFragment(shoe.name, shoe.size.toString(), shoe.description, shoe.id))
                }
            }
        })

        binding.newShoeButton.setOnClickListener{view: View ->
            findNavController(this).navigate(ShoeListFragmentDirections.actionShoeListFragmentToNewShoe())
        }

        userViewModel.userState.observe(viewLifecycleOwner, Observer { status ->

            when (status) {

                0 -> {
                    val action = ShoeListFragmentDirections.actionShoeListFragmentToLoginFragment()
                    findNavController(this).navigate(action)
                }

                1 -> {
                    val action = ShoeListFragmentDirections.actionShoeListFragmentToTitleFragment()
                    findNavController(this).navigate(action)
                }

                2 -> {
                    val action = ShoeListFragmentDirections.actionShoeListFragmentToInstructionsFragment()
                    findNavController(this).navigate(action)
                }

            }
        })

        return binding.root






    }


}