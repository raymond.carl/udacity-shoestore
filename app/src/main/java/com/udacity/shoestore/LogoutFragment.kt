package com.udacity.shoestore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.udacity.shoestore.databinding.FragmentLogoutBinding
import com.udacity.shoestore.models.UserViewModel

class LogoutFragment : Fragment() {

    private val userViewModel: UserViewModel by viewModels(
        ownerProducer = { requireParentFragment()}
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentLogoutBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_logout, container, false)


        binding.backButton.setOnClickListener { view:View ->
            Navigation.findNavController(requireView()).navigate(R.id.action_logoutFragment_to_shoeListFragment)
        }

        binding.logoutButton.setOnClickListener { view:View ->
            userViewModel.logOut()
            Navigation.findNavController(requireView()).navigate(R.id.action_logoutFragment_to_shoeListFragment)
        }

        return binding.root
    }


}