package com.udacity.shoestore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import com.udacity.shoestore.databinding.FragmentLoginBinding
import com.udacity.shoestore.models.UserViewModel

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation.findNavController


class LoginFragment : Fragment() {

    // Activity view model: https://developer.android.com/codelabs/basic-android-kotlin-training-shared-viewmodel
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentLoginBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_login, container, false)

        // Using this caused a reinitialization of UserViewModel
        //userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        // Auto logout if the user is sent to this screen
        //userViewModel.logOut()




        binding.buttonLogin.setOnClickListener { view: View ->
            userViewModel.logIn()
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_shoeListFragment)
        }

        binding.buttonRegister.setOnClickListener { view: View ->
            userViewModel.logIn()
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_shoeListFragment)
        }

        return binding.root
    }

}