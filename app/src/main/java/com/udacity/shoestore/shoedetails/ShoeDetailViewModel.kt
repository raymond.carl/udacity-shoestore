package com.udacity.shoestore.shoedetails

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.udacity.shoestore.models.Shoe
import com.udacity.shoestore.models.ShoeViewModel
import kotlinx.android.synthetic.main.shoe_detail_layout.view.*
import timber.log.Timber


class ShoeDetailViewModel (shoeDetails: Shoe, shoes: ShoeViewModel) : ViewModel() {

    private var shoes: ShoeViewModel = shoes

    private var _shoe = MutableLiveData<Shoe>()
    val shoe: LiveData<Shoe>
        get() = _shoe


    private var _navListings = MutableLiveData<Boolean>()
    val navListings: LiveData<Boolean>
        get() = _navListings

    init {
        _shoe.value = shoeDetails
        _navListings.value = false
    }

    fun cancel() {
        _navListings.value = true
    }

    fun saveShoe() {
        // This class can't be instantiated without a Shoe object. _shoe is an optional but updateshoe requires shoe non null
        shoes.updateShoe(_shoe.value!!)
        _navListings.value = true
    }


}