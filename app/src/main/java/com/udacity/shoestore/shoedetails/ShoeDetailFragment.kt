package com.udacity.shoestore.shoedetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation.findNavController
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.FragmentShoeDetailBinding
import com.udacity.shoestore.models.Shoe
import com.udacity.shoestore.models.ShoeViewModel

import com.udacity.shoestore.shoedetails.ShoeDetailViewModel
import androidx.navigation.fragment.NavHostFragment.findNavController

class ShoeDetailFragment : Fragment() {

    /*
    Technique was learned from this stackoverflow thread to access the same instance of the shoe viewmodel from a
    a "child" fragment (or fragment within the same activity) because I don't want to create a new instance
    of the shoe viemodel but we also aren't using repositories yet either. This is not a good pattern, but fits
    ok for this project.
    https://stackoverflow.com/questions/59952673/how-to-get-an-instance-of-viewmodel-in-activity-in-2020-21
    */

    val shoeViewModel: ShoeViewModel by viewModels(
        ownerProducer = { requireParentFragment() }
    )

    lateinit var currentShoe: Shoe

    private lateinit var shoeDetailViewModel: ShoeDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentShoeDetailBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_shoe_detail, container, false)

        var args = ShoeDetailFragmentArgs.fromBundle(requireArguments())

        currentShoe = shoeViewModel.getShoe(args.id)

        shoeDetailViewModel = ShoeDetailViewModel(currentShoe, shoeViewModel)


        binding.lifecycleOwner = this
        binding.shoe = shoeDetailViewModel

        shoeDetailViewModel.navListings.observe(viewLifecycleOwner, Observer { status ->
            if(status == true) {
                findNavController(this).navigate(ShoeDetailFragmentDirections.actionShoeDetailFragmentToShoeListFragment())
            }
        })


        return binding.root
    }

}