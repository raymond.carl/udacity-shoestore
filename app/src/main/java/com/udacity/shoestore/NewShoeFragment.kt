package com.udacity.shoestore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.udacity.shoestore.databinding.FragmentNewShoeBinding
import com.udacity.shoestore.models.Shoe
import com.udacity.shoestore.models.ShoeViewModel
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import kotlinx.android.synthetic.main.fragment_new_shoe.*
import kotlinx.android.synthetic.main.fragment_new_shoe.company
import kotlinx.android.synthetic.main.fragment_new_shoe.name
import kotlinx.android.synthetic.main.fragment_new_shoe.size
import kotlinx.android.synthetic.main.fragment_shoe_detail.*
import kotlinx.android.synthetic.main.shoe_detail_layout.*
import timber.log.Timber
import javax.xml.validation.Validator


class NewShoeFragment : Fragment() {


    /*
   Technique was learned from this stackoverflow thread to access the same instance of the shoe viewmodel from a
   a "child" fragment (or fragment within the same activity) because I don't want to create a new instance
   of the shoe viemodel but we also aren't using repositories yet either. This is not a good pattern, but fits
   ok for this project.
   https://stackoverflow.com/questions/59952673/how-to-get-an-instance-of-viewmodel-in-activity-in-2020-21
   */

    private val shoeViewModel: ShoeViewModel by viewModels(
        ownerProducer = { requireParentFragment()}
    )




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentNewShoeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_shoe, container, false)

        binding.returnButton.setOnClickListener{ view:View ->
            view.findNavController().navigate(NewShoeFragmentDirections.actionNewShoeToShoeListFragment())
        }

        binding.saveButton.setOnClickListener{ view: View ->

            var entryCheck = shoeViewModel.createNewShoe(binding.name.text.toString(),binding.company.text.toString(),binding.shoeDescription.text.toString(),binding.size.text.toString())

            if(entryCheck.containsKey(true)){
                Toast.makeText(getActivity(),entryCheck.get(true), Toast.LENGTH_LONG).show()
                view.findNavController().navigate(NewShoeFragmentDirections.actionNewShoeToShoeListFragment())
            } else {
                Toast.makeText(getActivity(),entryCheck.get(false), Toast.LENGTH_LONG).show()
            }




        }




        return binding.root

    }





}