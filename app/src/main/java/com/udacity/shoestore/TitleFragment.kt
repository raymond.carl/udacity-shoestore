package com.udacity.shoestore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.databinding.DataBindingUtil
import com.udacity.shoestore.databinding.FragmentTitleBinding
import androidx.navigation.Navigation
import com.udacity.shoestore.models.UserViewModel

import androidx.navigation.fragment.NavHostFragment.findNavController

import androidx.fragment.app.activityViewModels


import timber.log.Timber


class TitleFragment : Fragment() {

    private val userViewModel : UserViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentTitleBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_title, container, false)


        //val viewModelFactory = ShoeViewModelFactory()
        //val shoeViewModel = ViewModelProvider(this, viewModelFactory).get(ShoeViewModel::class.java)

        binding.titleButton.setOnClickListener{
            userViewModel.viewedTitle()
            findNavController(this).navigate(TitleFragmentDirections.actionTitleFragmentToShoeListFragment())
        }

        return binding.root
    }





}