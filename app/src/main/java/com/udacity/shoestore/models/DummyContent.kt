package com.udacity.shoestore.models

import com.udacity.shoestore.models.Shoe
import java.util.ArrayList
import java.util.HashMap


object DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    //val ITEMS: MutableList<DummyItem> = ArrayList()
    val Shoes: MutableList<Shoe> = ArrayList()

    /**
     * A map of sample (dummy) items, by ID.
     */
    //val ITEM_MAP: MutableMap<String, DummyItem> = HashMap()
    val Shoe_map: MutableMap<String, Shoe> = HashMap()

    private val COUNT = 10

    init {
        // Add some sample items.
        for (i in 1..COUNT) {
            //addItem(createDummyItem(i))
            addShoe(createDummyShoe(i))
        }
    }

    /*
    private fun addItem(item: DummyItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id, item)
    }
     */

    private fun addShoe(item: Shoe) {
        Shoes.add(item)
        Shoe_map.put(item.id, item)
    }

    /*
    private fun createDummyItem(position: Int): DummyItem {
        return DummyItem(position.toString(), "Item " + position, makeDetails(position))
    }
    */

    private fun createDummyShoe(position: Int): Shoe {
        val imageList = listOf("https://1.com", "https://2.com", "https://3.com")
        return Shoe(name="MaxAir 10", size=10.1, company="Jill's", description="New Sport's Shoe", images=imageList, id=position.toString())
    }

    /*
    private fun makeDetails(position: Int): String {
        val builder = StringBuilder()
        builder.append("Details about Item: ").append(position)
        for (i in 0..position - 1) {
            builder.append("\nMore details information here.")
        }
        return builder.toString()
    }
     */

    /**
     * A dummy item representing a piece of content.
     */
    /*
    data class DummyItem(val id: String, val content: String, val details: String) {
        override fun toString(): String = content
    }

     */




}
