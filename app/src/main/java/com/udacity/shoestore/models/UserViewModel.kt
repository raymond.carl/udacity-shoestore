package com.udacity.shoestore.models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import timber.log.Timber


class UserViewModel : ViewModel() {

    // Boolean to represent whether the user has visited the login screen or not. Actual login not required for this project.



    private val _userState = MutableLiveData<Int>()
    val userState: LiveData<Int>
        get() = _userState

    init {
        _userState.value = 0
    }


    fun logIn() {
        _userState.value = 1
    }

    fun logOut() {
        _userState.value = 0
    }

    fun viewedTitle() {
        _userState.value = 2
    }

    fun viewedInstructions() {
        _userState.value = 3
    }



}