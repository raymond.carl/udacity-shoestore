package com.udacity.shoestore.models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import java.util.ArrayList
import androidx.lifecycle.ViewModel
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import timber.log.Timber


class ShoeViewModel : ViewModel() {


    private var _shoes = MutableLiveData<MutableList<Shoe>>()

    // !?!?!?! Can Livedata with a mutabelist be mutated !?!?!
    val shoes : LiveData<MutableList<Shoe>>
        get() = _shoes


    /*

    Attempt #2: https://stackoverflow.com/questions/47941537/notify-observer-when-item-is-added-to-list-of-livedata
    private val _shoes = ArrayList<Shoe>()
    val shoes : MutableLiveData<List<Shoe>>
        get() = _shoes
    */

    private val count = 10

    init {

        _shoes.value = ArrayList()

        for (i in 0..count) {
            addShoe(createDummyShoe(i))
        }
    }

    private fun addShoe(item: Shoe) {
        _shoes.value?.add(item)
    }

    private fun createDummyShoe(position: Int): Shoe {
        val imageList = listOf("https://1.com", "https://2.com", "https://3.com")
        return Shoe(name="MaxAir 10", size=10.1, company="Jill's", description="New Sport's Shoe", images=imageList, id=position.toString())
    }


    /*
    Struggled with finding an easy way to replace an entire element within mutablelist wrapped within
    mutable live data. Settled on this approach which replaces each property individually within the element .
    https://stackoverflow.com/questions/54797411/change-a-value-in-mutable-list-in-kotlin
     */
    fun updateShoe(updatedShoe: Shoe){
        _shoes.value?.find { it.id == updatedShoe.id }?.description = updatedShoe.description
        _shoes.value?.find { it.id == updatedShoe.id }?.size = updatedShoe.size
        _shoes.value?.find { it.id == updatedShoe.id }?.name = updatedShoe.name
        _shoes.value?.find { it.id == updatedShoe.id }?.company = updatedShoe.company
    }

    fun getShoe(index: String): Shoe{

        // Should not use the not-null assertion operator, but difficult area to deal with given the project context.
        return _shoes.value!!.find { it.id == index }!!

    }

    fun createNewShoe(name: String, company: String, description: String, size: String): MutableMap<Boolean, String>{

    /*
    Create a new index that avoids a collision with the existing indexes from the dummy data creation process.
    Note: there is no possibility to delete shoes in this version of the app
     */

        val imageList = listOf("https://1.com", "https://2.com", "https://3.com")
        val status: MutableMap<Boolean, String> = mutableMapOf()
        var doubleSize: Double

        // n+1 problem is avoided here because index starts at 0 for kotlin
        var newIndex = _shoes.value?.size

        if(name.isBlank()) {
            status.put(key = false, value = "The Name Field is empty. Please insert a name.")
            return status
        }  else if (description.isBlank()) {
            status.put(key = false, value = "The Description Field is empty. Please insert a description.")
            return status
        } else if (company.isBlank()) {
            status.put(key = false, value = "The Company Field is empty. Please insert a company.")
            return status
        }

        try {
            doubleSize = size.toDouble()
        }catch (e:  NumberFormatException){
            status.put(key = false, value = "Please enter a valid shoe size in 10.X format such as 9.5 or 12.0.")
            return status
        }

        var newShoe = Shoe(name,doubleSize,company,description,imageList,newIndex.toString())

        addShoe(newShoe)
        status.put(key =true, value = "Your New Shoe Has Been Added to the Catalogue!")

        return status




    }
}